package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.model.Test;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		appController.loadIntrebariFromFile(file);
		
		boolean activ = true;
		String optiune = null;
		
		while(activ){

			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");

			optiune = console.readLine();

			switch(optiune){
				case "1" :
					System.out.println("Enunt intrebare:");
					String enunt = console.readLine();
					System.out.println("Raspuns 1:");
					String raspuns1 = console.readLine();
					System.out.println("Raspuns 2:");
					String raspuns2 = console.readLine();
					System.out.println("Raspunsul corect:");
					String raspunsCorect = console.readLine();
					System.out.println("Domeniul:");
					String domeniu = console.readLine();
					try {
						appController.addNewIntrebare(new Intrebare(enunt,raspuns1,raspuns2,raspunsCorect,domeniu));
					} catch (DuplicateIntrebareException | InputValidationFailedException e) {
						System.out.println(e.getMessage());
					}
					break;
				case "2" :
					try {
						Test test = appController.createNewTest();
						System.out.println(test.toString());
					} catch (NotAbleToCreateTestException e) {
						System.out.println(e.getMessage());
					}
					break;
				case "3" :
					//appController.loadIntrebariFromFile(file);
					Statistica statistica;
					try {
						statistica = appController.getStatistica();
						System.out.println(statistica);
					} catch (NotAbleToCreateStatisticsException e) {
						System.out.println(e.getMessage());
					}

					break;
				case "4" :
					activ = false;
					break;
				default:
					break;
			}

		}
		
	}

}
